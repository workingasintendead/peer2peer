from django.shortcuts import render, get_object_or_404, redirect
from django.http.request import HttpRequest
from django.http.response import HttpResponse
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def show_recipe(request: HttpRequest, id: int) -> HttpResponse:
    recipe = get_object_or_404(Recipe, id=id)
    context = {"recipe_object": recipe}
    # every key in the context dictionary is a variable in the HTML template.
    return render(request, "recipes/detail.html", context)


def reciepe_list(request):
    recipes = Recipe.objects.all()
    context = {"recipe_list": recipes}
    return render(request, "recipes/list.html", context)


@login_required
def create_recipe(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            # saves without commiting to database yet
            recipe: Recipe = form.save(commit=False)
            # adds user as author to the form if logged in
            recipe.author = request.user
            # actually saves to database
            recipe.save()
            return redirect("recipes_list")
    else:
        form = RecipeForm()
        context = {"recipe_form": form}
    return render(request, "recipes/create.html", context)


@login_required
def edit_recipe(request: HttpRequest, id: int) -> HttpResponse:
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe")
    else:
        form = RecipeForm(instance=recipe)
        context = {"recipe_form": form}
    return render(request, "recipes/edit.html", context)


@login_required
def my_reciepe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {"recipe_list": recipes}
    return render(request, "recipes/list.html", context)
