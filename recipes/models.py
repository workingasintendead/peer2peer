from django.db import models
from django.conf import settings


# Create your models here.
class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    rating = models.CharField(max_length=5)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               related_name="recipes",
                               on_delete=models.PROTECT,
                               null=True)

    def __str__(self) -> str:
        return self.title

    # anytime changing the models YOU MUST MAKEMIGRATIONS then MIGRATE!


class RecipeStep(models.Model):
    instruction = models.TextField()
    order = models.PositiveIntegerField()
    Recipe = models.ForeignKey("Recipe",
                               related_name="recipe_steps",
                               on_delete=models.CASCADE)

    class Meta:
        ordering = ["order"]
        # extra magic that sorts them by the order above

    def recipe_title(self):
        return self.Recipe.title

    def __str__(self) -> str:
        return f"{self.Recipe.title} - Step: {self.order}"


class Ingredient(models.Model):
    amount = models.CharField(max_length=200)
    food_item = models.CharField(max_length=200)
    Recipe = models.ForeignKey("Recipe",
                               related_name="ingredients",
                               on_delete=models.CASCADE)

    def recipe_title(self):
        return self.Recipe.title

    def __str__(self) -> str:
        return f"{self.Recipe.title} - Ingredient"
