# Generated by Django 5.0.6 on 2024-05-16 17:46

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='RecipeStep',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('instruction', models.TextField()),
                ('order', models.PositiveIntegerField()),
                ('Recipe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='recipe_steps', to='recipes.recipe')),
            ],
            options={
                'ordering': ['order'],
            },
        ),
    ]
