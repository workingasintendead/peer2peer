from django.contrib import admin
from .models import Recipe, RecipeStep, Ingredient

# Register your models here.
admin.site.register(Recipe)

admin.site.register(RecipeStep)

admin.site.register(Ingredient)
